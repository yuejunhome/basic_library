package com.jun.BasicLibrary

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import com.jun.jun_util.BaseApplication
import com.jun.jun_util.util.toastutil.ToastU

class MainActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        BaseApplication.init(application)
        ToastU.shortToast("llkjs")
    }
}