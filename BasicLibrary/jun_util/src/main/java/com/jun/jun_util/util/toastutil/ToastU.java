package com.jun.jun_util.util.toastutil;

import android.text.TextUtils;
import android.view.Gravity;
import android.view.View;
import android.widget.Toast;

import com.google.android.material.snackbar.Snackbar;
import com.jun.jun_util.BaseApplication;


public class ToastU {

    private static long toastTime = 0;

    public static void shortToast(String msg) {
        if (TextUtils.isEmpty(msg)){
            msg = "服务器出小差了，请稍候再试~";
        }
        if (System.currentTimeMillis() - toastTime > 1000) {
            toastTime = System.currentTimeMillis();
            Toast toast = Toast.makeText(BaseApplication.getApp(), msg, Toast.LENGTH_SHORT);
            toast.setText(msg);
            toast.setGravity(Gravity.CENTER, 0, 0);
            toast.show();

        }
    }

    public static void shortToast(View view, String msg) {
        if (TextUtils.isEmpty(msg)){
            msg = "服务器出小差了，请稍候再试~";
        }
        if (System.currentTimeMillis() - toastTime > 1000) {
            toastTime = System.currentTimeMillis();
            Snackbar.make( view,msg, Snackbar.LENGTH_SHORT).show();

        }
    }

    public static void shortToast(int msg) {
        if (System.currentTimeMillis() - toastTime > 1000) {
            toastTime = System.currentTimeMillis();
            Toast toast = Toast.makeText(BaseApplication.getApp(), msg, Toast.LENGTH_SHORT);
            toast.setText(msg);
            toast.setGravity(Gravity.CENTER, 0, 0);
            toast.show();
        }
    }

    public static void longToast(String msg) {
        if (System.currentTimeMillis() - toastTime > 1000) {
            if (TextUtils.isEmpty(msg)){
                msg = "服务器出小差了，请稍候再试~";
            }
            toastTime = System.currentTimeMillis();
            Toast toast = Toast.makeText(BaseApplication.getApp(), msg, Toast.LENGTH_LONG);
            toast.setText(msg);
            toast.setGravity(Gravity.CENTER, 0, 0);
            toast.show();
        }
    }
}
