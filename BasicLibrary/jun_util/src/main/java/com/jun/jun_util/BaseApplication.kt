package com.jun.jun_util

import android.app.Activity
import android.app.Application

import java.util.*

/**
 * Description: BaseApplication
 * History:
 */
val mApp by lazy { BaseApplication.app }

object BaseApplication {

    val TAG = "BaseApplicationTAG"

    @JvmStatic
    lateinit var app: Application
        private set





    @JvmStatic
    fun init(application: Application) {

        app = application
        //监听app前后台状态


    }



}